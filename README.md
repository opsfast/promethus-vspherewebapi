# promethus-vspherewebapi-go

  当前需求有100多台EXSI 和 多台VCenter，写了一个通用的prometheus vmware go客户端
之前使用python写过，因为python有依赖包的问题，所以又写了一个go，使用起来方便。

#主要监控指标，

| 性能指标          | 虚拟机 | ESXI |
| :---------------- | ------ | ---- |
| cpu总量           |        | 有   |
| cpu使用           | 有     | 有   |
| 内存总            | 有     | 有   |
| 内存使用          | 有     | 有   |
| 磁盘总量          | 有     | 有   |
| 磁盘使用          | 有     | 有   |
| 运行时间          | 有     | 有   |
| 网络流量接受/发送 | 有     | 有   |
| 虚拟机数量        |        | 有   |

#使用方法，

./govsphere -user XX  -password XX  -vcip XX  -metricsport XX

user:EXSI或者Vcenter的用户名
password：EXSI或者Vcenter的密码
vcip：EXSI或者Vcenter的IP
metricsport：在你电脑上开启的端口

#打开方式
http://你电脑的ip:metricsport

更新了说明