package vms

import (
	"context"
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/vmware/govmomi/performance"
	"github.com/vmware/govmomi/view"
	"github.com/vmware/govmomi/vim25"
	"github.com/vmware/govmomi/vim25/mo"
	"github.com/vmware/govmomi/vim25/types"
	"log"
	"strconv"
)

var (
	vmNet = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "vms_net",
		Help: "网卡流量 (KB) kiloBytesPerSecond",
	}, []string{"vm", "interface", "type"},
	)
)

func init() {
	// Metrics have to be registered to be exposed:
	prometheus.MustRegister(vmNet)
}
func GetEsxipre(c *vim25.Client) error {
	// Get virtual machines references
	ctx := context.Background()
	m := view.NewManager(c)
	v, err := m.CreateContainerView(ctx, c.ServiceContent.RootFolder, []string{"VirtualMachine"}, true)
	if err != nil {
		panic(err)
	}
	defer v.Destroy(ctx)
	var vms []mo.VirtualMachine
	err = v.Retrieve(ctx, []string{"VirtualMachine"}, []string{"summary", "guest", "triggeredAlarmState"}, &vms)
	if err != nil {
		log.Fatal(err)
	}
	//建立主机名和标识的键值对
	hostListMap := make(map[string]string)
	for _, vm := range vms {
		hostListMap[vm.Summary.Vm.Value] = vm.Summary.Config.Name
		//fmt.Println()
	}
	//创建主机清单，用于后面主机标签对应
	vmsRefs, err := v.Find(ctx, []string{"VirtualMachine"}, nil)
	if err != nil {
		fmt.Println(err)
	}
	// Create a PerfManager
	perfManager := performance.NewManager(c)
	var names []string
	//先只加网卡的接受和发包
	names = append(names, "net.received.average")
	names = append(names, "net.transmitted.average")
	// Create PerfQuerySpec
	spec := types.PerfQuerySpec{
		MaxSample:  1,
		MetricId:   []types.PerfMetricId{{Instance: "*"}},
		IntervalId: 20,
	}
	// Query metrics
	sample, err := perfManager.SampleByName(ctx, spec, names, vmsRefs)
	if err != nil {
		fmt.Println(err)
	}
	result, err := perfManager.ToMetricSeries(ctx, sample)
	if err != nil {
		fmt.Println(err)
	}
	// Read result
	for _, metric := range result {
		name := metric.Entity
		if err != nil {
			fmt.Println(err)
		}
		for _, v := range metric.Value {
			instance := v.Instance
			if instance == "" {
				instance = "-"
			}
			if len(v.Value) != 0 {
				//fmt.Printf("%s\t%s\t%s\t%s\n", hostListMap[name.Value], instance, v.Name, v.ValueCSV())
				mervalue, _ := strconv.ParseFloat(v.ValueCSV(), 64)
				vmNet.With(prometheus.Labels{"vm": hostListMap[name.Value], "interface": instance, "type": v.Name}).Set(mervalue)
			}
		}
	}
	return nil
}
